/**
 * author: Soli
 * date  : 2012-05-19
 * */

#ifndef __SLOG_H__
#define __SLOG_H__

#ifdef __cplusplus
extern "C" {
#endif


/* add #include here */
#include <stdio.h>
#include <pthread.h>
#include <limits.h>

/**
 * log level
 * */
typedef enum
{
	SLOG_LEVEL_NONE		= 0,
	SLOG_LEVEL_DATA		= 1,
	SLOG_LEVEL_ERR		= 2,
	SLOG_LEVEL_WARN		= 3,
	SLOG_LEVEL_INFO		= 4,
	SLOG_LEVEL_DEBUG	= 5,
	SLOG_LEVEL_UNKNOWN	= 6,
} slog_level;

/**
 * log scale
 * */
typedef enum
{
	SLOG_SCALE_MIN		= 1,
	SLOG_SCALE_HOUR		= 2,
	SLOG_SCALE_DAY		= 3,
	SLOG_SCALE_MON		= 4,
	SLOG_SCALE_YEAR		= 5,
	SLOG_SCALE_NONE		= 6,
} slog_scale;

/**
 * defaults and max
 * */
#define SLOG_SCALE_DEFAULT SLOG_SCALE_HOUR
#define SLOG_LEVEL_DEFAULT SLOG_LEVEL_INFO
#define SLOG_LINE_MAX 1024

/**
 * log struct
 * */
typedef struct slog_s
{
	FILE *fp;
	char file_name[PATH_MAX];
	time_t ren_t;
	slog_level lvl;
	slog_scale scale;
	pthread_mutex_t lock;
} slog_t;

/**
 * open a log using default params, and return the handle.
 * */
slog_t* slog_open(const char *name);

/**
 * open a log, and return the handle.
 * */
slog_t* slog_open_ex(const char *name, const char *path, slog_scale scale);

/**
 * set log level
 * */
void slog_setlvl(slog_t * log, slog_level lvl);

/**
 * set log level by level name
 * */
void slog_setlvl_byname(slog_t * log, const char * lvl);

/**
 * write log
 * */
void slog_write(slog_t * log, slog_level lvl, const char *format, ...);

/**
 * close the log.
 * */
void slog_close(slog_t * log);

/**
 * smart macros
 * */
#define SLOG_DATA(log, f, args...)		slog_write(log, SLOG_LEVEL_DATA, f, ##args)
#define SLOG_ERR(log, f, args...)		slog_write(log, SLOG_LEVEL_ERR, f, ##args)
#define SLOG_WARN(log, f, args...)		slog_write(log, SLOG_LEVEL_WARN, f, ##args)
#define SLOG_INFO(log, f, args...)		slog_write(log, SLOG_LEVEL_INFO, f, ##args)
#define SLOG_DEBUG(log, f, args...)		slog_write(log, SLOG_LEVEL_DEBUG, "<"__FILE__":"__LINE__":"__FUNCTION__"> "f, ##args)
#define SLOG_UNKNOWN(log, f, args...)	slog_write(log, SLOG_LEVEL_UNKNOWN, "<"__FILE__":"__LINE__":"__FUNCTION__"> "f, ##args)

#ifdef __cplusplus
}
#endif

#endif	/* #ifndef __SLOG_H__ */

