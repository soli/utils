/**
 * author: Soli
 * date  : 2012-05-19
 * */

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/stat.h>

#include "slog.h"

static const char *slog_level_names[] = {
	[SLOG_LEVEL_NONE]	= "!!!",
	[SLOG_LEVEL_DATA]	= "DATA",
	[SLOG_LEVEL_ERR]	= "ERROR",
	[SLOG_LEVEL_WARN]	= "WARN",
	[SLOG_LEVEL_INFO]	= "INFO",
	[SLOG_LEVEL_DEBUG]	= "DEBUG",
	[SLOG_LEVEL_UNKNOWN]= "UNKOWN",
};


static int get_app_path(char * app_path);
static time_t next_ren_time(slog_scale scale);
static int slog_postfix(time_t t, slog_scale scale, char *postfix, size_t maxsize);
static void slog_ren(slog_t * log);
static int slog_do_write(slog_t * log, const char * log_line, size_t log_len);
static void slog_flush(slog_t * log);

/**
 * open a log using default params, and return the handle.
 * */
slog_t* slog_open(const char *name)
{
	char path[PATH_MAX];
	int path_len = get_app_path(path);

	if(path_len < 0 || path_len >= PATH_MAX)
	{
		return NULL;
	}

	path[path_len] = '\0';

	strcat(path, "logs");

	return slog_open_ex(name, path, SLOG_SCALE_DEFAULT);
}

/**
 * open a log, and return the handle.
 * */
slog_t* slog_open_ex(const char *name, const char *path, slog_scale scale)
{
	if(NULL == name)
	{
		return NULL;
	}

	slog_t * log = (slog_t*)malloc(sizeof(slog_t));
	if(NULL == log)
	{
		return NULL;
	}

	/* 试着创建目录，不管成功与否，关键看下一步是否能打开文件 */
	mkdir(path, 0755);

	strcpy(log->file_name, path);
	strcat(log->file_name, "/");
	strcat(log->file_name, name);

	log->fp = fopen(log->file_name, "a");

	if(NULL == log->fp)
	{
		free(log);
		return NULL;
	}

	log->lvl = SLOG_LEVEL_DEFAULT;
	log->scale = scale;
	log->ren_t = next_ren_time(scale);
	pthread_mutex_init(&(log->lock), NULL);

	return log;
}

/**
 * set log level
 * */
void slog_setlvl(slog_t * log, slog_level lvl)
{
	if(NULL != log)
	{
		log->lvl = lvl;
	}
}

/**
 * set log level by level name
 * */
void slog_setlvl_byname(slog_t * log, const char * lvl)
{
	slog_level l = SLOG_LEVEL_NONE;
	for( ; l < SLOG_LEVEL_UNKNOWN; ++l)
	{
		if(0 == strcmp(slog_level_names[l], lvl))
		{
			break;
		}
	}
	
	slog_setlvl(log, l);
}

/**
 * write log
 * example:
 * [2012-05-19 19:12:12] [INFO] I'm a log.
 * */
void slog_write(slog_t * log, slog_level lvl, const char *format, ...)
{
	time_t cur = time(NULL);
	char log_line[SLOG_LINE_MAX];
	char time_str[32];
	size_t log_len = 0;
	struct tm tm_s;

	if(NULL == log || NULL == log->fp || NULL == format || lvl > log->lvl)
	{
		return;
	}

	if((int)lvl < (int)SLOG_LEVEL_NONE)
	{
		lvl = SLOG_LEVEL_NONE;
	}
	else if((int)lvl > (int)SLOG_LEVEL_UNKNOWN)
	{
		lvl = SLOG_LEVEL_UNKNOWN;
	}

	log_len = strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", localtime_r(&cur, &tm_s));
	time_str[log_len] = '\0';

	log_len = snprintf(log_line, SLOG_LINE_MAX, "[%s] [%s] ", time_str, slog_level_names[lvl]);

	if(log_len < SLOG_LINE_MAX)
	{
		va_list args;
		va_start(args, format);
		log_len += vsnprintf(log_line+log_len, SLOG_LINE_MAX-log_len, format, args);
		va_end(args);
	}

	if(log_len >= SLOG_LINE_MAX)
	{
		log_len = SLOG_LINE_MAX - 1;

		strcpy(log_line + SLOG_LINE_MAX - 5, " ...");
	}
	log_line[log_len++] = '\n';

	// 如果没到改名时间，直接放入当前日志文件。
	if(cur > log->ren_t && log->ren_t > 0)
	{
		slog_ren(log);
	}

	if( 0 != slog_do_write(log, log_line, log_len))
	{
		perror("fwrite()");
	}
}

/**
 * close the log.
 * */
void slog_close(slog_t * log)
{
	if(NULL == log)
	{
		return;
	}

	pthread_mutex_lock(&(log->lock));

	if(NULL != log->fp)
	{
		slog_flush(log);
		fclose(log->fp);
		log->fp = NULL;
	}

	pthread_mutex_unlock(&(log->lock));

	free(log);
}

/**
 * write log to file
 * */
static int slog_do_write(slog_t * log, const char * log_line, size_t log_len)
{
	int ret = 0;

	pthread_mutex_lock(&(log->lock));

	if(1 != fwrite(log_line, log_len, 1, log->fp))
	{
		ret = 1;
	}

	slog_flush(log);

	pthread_mutex_unlock(&(log->lock));

	return ret;
}

/**
 * flush buf to file
 * */
static void slog_flush(slog_t * log)
{
	fflush(log->fp);
}

static void slog_ren(slog_t * log)
{
	char new_name[PATH_MAX];
	char postfix[PATH_MAX];

	slog_postfix(log->ren_t, log->scale, postfix, PATH_MAX);

	pthread_mutex_lock(&(log->lock));

	if(time(NULL) <= log->ren_t)
	{
		pthread_mutex_unlock(&(log->lock));
		return;
	}

	slog_flush(log);
	fclose(log->fp);
	log->fp = NULL;

	int i = 0;
	for( ; i < 100; ++i)
	{
		if(0 == i)
		{
			snprintf(new_name, PATH_MAX, "%s%s", log->file_name, postfix);
		}
		else
		{
			snprintf(new_name, PATH_MAX, "%s%s.%d", log->file_name, postfix, i);
		}

		if(0 == link(log->file_name, new_name))
		{
			unlink(log->file_name);
			break;
		}
		else if(EEXIST != errno)
		{
			break;
		}
	}

	log->fp = fopen(log->file_name, "a");
	log->ren_t = next_ren_time(log->scale);

	pthread_mutex_unlock(&(log->lock));
}

static int get_app_path(char * app_path)
{
	char proc_exe[] = "/proc/self/exe";
	ssize_t len = readlink(proc_exe, app_path, PATH_MAX);

	if(len > 0)
	{
		app_path[len] = '\0';

		char *ptr = strrchr(app_path, '/');
		if(ptr)
		{
			ptr[1]='\0';
			return (ptr - app_path + 1);
		}
	}

	return -1;
}

static time_t next_ren_time(slog_scale scale)
{
	time_t cur = time(NULL);

	if(SLOG_SCALE_MIN == scale)
	{
		return (cur / 60 + 1) * 60;
	}
	else if(SLOG_SCALE_HOUR == scale)
	{
		return (cur / 3600 + 1) * 3600;
	}
	else if(SLOG_SCALE_DAY == scale)
	{
		return (cur / (3600 * 24) + 1) * (3600 * 24);
	}
	else if(SLOG_SCALE_MON == scale)
	{
		struct tm tm_s;
		localtime_r(&cur, &tm_s);
		tm_s.tm_sec = 0;
		tm_s.tm_min = 0;
		tm_s.tm_hour = 0;
		tm_s.tm_mday = 1;
		tm_s.tm_mon++;
		if(tm_s.tm_mon > 11)
		{
			tm_s.tm_mon = 0;
			tm_s.tm_year++;
		}

		return mktime(&tm_s);
	}
	else if(SLOG_SCALE_YEAR == scale)
	{
		struct tm tm_s;
		localtime_r(&cur, &tm_s);
		tm_s.tm_sec = 0;
		tm_s.tm_min = 0;
		tm_s.tm_hour = 0;
		tm_s.tm_mday = 1;
		tm_s.tm_mon = 0;
		tm_s.tm_year++;

		return mktime(&tm_s);
	}
	else
	{
		return -1;
	}
}

static int slog_postfix(time_t t, slog_scale scale, char *postfix, size_t maxsize)
{
	size_t len = 0;
	struct tm tm_s;

	if(t < 0)
	{
		postfix[0] = '\0';

		return 0;
	}

	t--;

	if(SLOG_SCALE_MIN == scale)
	{
		len = strftime(postfix, maxsize, ".%Y-%m-%d-%H-%M", localtime_r(&t, &tm_s));
	}
	else if(SLOG_SCALE_HOUR == scale)
	{
		len = strftime(postfix, maxsize, ".%Y-%m-%d-%H", localtime_r(&t, &tm_s));
	}
	else if(SLOG_SCALE_DAY == scale)
	{
		len = strftime(postfix, maxsize, ".%Y-%m-%d", localtime_r(&t, &tm_s));
	}
	else if(SLOG_SCALE_MON == scale)
	{
		len = strftime(postfix, maxsize, ".%Y-%m", localtime_r(&t, &tm_s));
	}
	else if(SLOG_SCALE_YEAR == scale)
	{
		len = strftime(postfix, maxsize, ".%Y", localtime_r(&t, &tm_s));
	}
	else
	{
		len = 0;
	}

	postfix[len] = '\0';

	return len;
}

