#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>

#include "slog.h"

int g_log_cnt = 1000;
int g_log_lvl = SLOG_LEVEL_DEFAULT;

void* worker(void *p)
{
	slog_t * log = (slog_t*)p;

	pthread_t tid = pthread_self();

	int i = 0;
	for( ; i < g_log_cnt; ++i)
	{
		struct timeval tv;
		gettimeofday(&tv, 0);

		slog_write(log, i % SLOG_LEVEL_UNKNOWN, "[%d] [%05d] [%d.%06d] some log.", tid, i, tv.tv_sec, tv.tv_usec);
	}

	return NULL;
}

int main(int argc, char * argv[])
{
	if(argc < 3)
	{
		printf("usage: %s <log count> <scale> [log level]\n", argv[0]);
		return 1;
	}

	g_log_cnt = atoi(argv[1]);
	int log_scale = atoi(argv[2]);

	if(argc > 3)
	{
		g_log_lvl = atoi(argv[3]);
	}

	slog_t * log = slog_open_ex("slog.log", "./logs", log_scale);

	if(NULL == log)
	{
		perror("slog_open()");
		return 1;
	}

	slog_setlvl(log, g_log_lvl);

	pthread_t ths[10];
	int i = 0;
	for( ; i < 10; ++i)
	{
		if(pthread_create(&ths[i], NULL, worker, (void*)log))
		{
			perror("pthread_create()");
			slog_close(log);
			return 1;
		}
	}

	for(i = 0; i < 10; ++i)
	{
		pthread_join(ths[i], NULL);
	}

	return 0;
}

