/**
 * author: Soli <soli@cbug.org>
 * date  : 2012-05-24
 * */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "../sevent.h"

char name1[] = "timer1";
char name2[] = "timer2";

time_t g_init_t;

void timer(int ed, int /*type*/, void * arg)
{
	static int i = 0;
	const char * name = (const char *)arg;
	printf("%ld[%d]: %s: %d\n", time(NULL) - g_init_t, ed, name, i++);
}

int main(int argc, char * argv[])
{
	if(argc < 3)
	{
		printf("usage: %s timeout1 timeout2\n", argv[0]);
		return 1;
	}

	SEvent events;
	if(0 != events.init(SEMOD_EPOLL, 3))
	{
		perror("events.init()");
		return 1;
	}

	events.add(make_timer(atoi(argv[1]), true, timer, name1));
	events.add(make_timer(atoi(argv[2]), true, timer, name2));

	g_init_t = time(NULL);

	/* main loop */
	events.loop();

	events.clean();
	return 0;
}
