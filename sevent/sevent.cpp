/**
 * author: Soli <soli@cbug.org>
 * date  : 2012-05-23
 * */

#include <stdlib.h>

#include "sevent.h"
#include "modules/sepoll.h"

Event make_timer(time_t timeout, bool persist, SEventCallback callback, void * arg)
{
	Event ev;

	ev.flags = SEV_TIMEOUT;

	if(persist)
	{
		ev.flags |= SEV_PERSIST;
	}

	gettimeofday(&(ev.timeout), NULL);

	ev.frequency.tv_sec = timeout / 1000;
	ev.frequency.tv_usec= (timeout % 1000) * 1000;

	timeradd(&(ev.timeout), &(ev.frequency), &(ev.timeout));
	
	ev.callback = callback;
	ev.arg = arg;

	return ev;
}

Event make_event(int fd, int flags, SEventCallback callback, void * arg)
{
	Event ev;

	ev.fd = fd;
	ev.flags = flags;
	ev.callback = callback;
	ev.arg = arg;

	return ev;
}

SEvent::SEvent()
:m_events_pool(NULL)
,m_max_events(0)
,m_free_eds(NULL)
,m_free_top(0)
,m_event_mod(NULL)
,m_running(false)
{
}

SEvent::~SEvent()
{
	clean();
}

int SEvent::init(SEventModType type, int max)
{
	if(max < 1)
	{
		return -2;
	}

	if(SEMOD_EPOLL == type)
	{
		m_event_mod = new SEpoll();
	}
	else if(SEMOD_SELECT == type)
	{
		//TODO: ...
	}
	else
	{
		return -1;
	}

	if(NULL == m_event_mod)
	{
		return -3;
	}

	if(0 != m_event_mod->init(max))
	{
		delete m_event_mod;
		m_event_mod = NULL;
		return -4;
	}

	
	m_events_pool = new Event[max];
	m_free_eds = new int[max];
	m_free_top = 0;
	m_max_events = max;

	m_events.clear();
	m_timers.clear();

	for( ; m_free_top < max; ++m_free_top)
	{
		m_free_eds[m_free_top] = m_free_top;
	}
	return 0;
}

void SEvent::clean(void)
{
	if(NULL != m_events_pool)
	{
		delete [] m_events_pool;
		m_events_pool = NULL;
	}

	if(NULL != m_free_eds)
	{
		delete [] m_free_eds;
		m_free_eds = NULL;
	}

	m_free_top = 0;
	m_events.clear();
	m_timers.clear();

	if(NULL != m_event_mod)
	{
		m_event_mod->clean();
		delete m_event_mod;
		m_event_mod = NULL;
	}
}

int SEvent::add(const Event &ev)
{
	if(m_free_top < 1)
	{
		return -1;
	}

	int ed = m_free_eds[--m_free_top];
	m_events_pool[ed] = ev;
	m_events_pool[ed].ed = ed;

	/* it's a triggered event */
	if (ev.flags & SEV_READ || ev.flags & SEV_WRITE)
	{
		if(NULL == m_event_mod || 0 != m_event_mod->add(&m_events_pool[ed]))
		{
			m_free_eds[m_free_top++] = ed;
			return -2;
		}
		m_events.insert(ed);
	}

	/* it's a timer event */
	if (ev.flags & SEV_TIMEOUT)
	{
		m_timers.insert(ed);
	}

	return ed;
}

int SEvent::mod(const Event &ev)
{
	int ed = ev.ed;

	if(0 == m_events.count(ed) && 0 == m_timers.count(ed))
	{
		return(-1);
	}

	Event old_ev = m_events_pool[ed];
	m_events_pool[ed] = ev;

	/* it's a triggered event */
	if(old_ev.flags & SEV_READ || old_ev.flags & SEV_WRITE)
	{
		if(old_ev.fd != ev.fd)
		{
			m_events_pool[ed] = old_ev;
			return(-2);
		}
		else if(ev.flags & SEV_READ || ev.flags & SEV_WRITE)
		{
			if(0 != m_event_mod->mod(&m_events_pool[ed]))
			{
				m_events_pool[ed] = old_ev;
				return(-3);
			}
		}
		else
		{
			if(0 != m_event_mod->del(&m_events_pool[ed]))
			{
				m_events_pool[ed] = old_ev;
				return(-4);
			}
		}
	}
	else if(ev.flags & SEV_READ || ev.flags & SEV_WRITE)
	{
		if(0 != m_event_mod->add(&m_events_pool[ed]))
		{
			m_events_pool[ed] = old_ev;
			return(-5);
		}
	}

	/* it's a timer event */
	if (ev.flags & SEV_TIMEOUT)
	{
		m_timers.insert(ed);
	}
	else
	{
		m_timers.erase(ed);
	}

	return(0);
}

int SEvent::del(int ed)
{
	bool is_del = false;
	if(m_events.count(ed))
	{
		m_event_mod->del(&m_events_pool[ed]);
		m_events.erase(ed);
		is_del = true;
	}

	if(m_timers.count(ed))
	{
		m_timers.erase(ed);
		is_del = true;
	}

	if(is_del)
	{
		m_free_eds[m_free_top++] = ed;
		return(0);
	}
	return(-1);
}

void SEvent::loop(void)
{
	m_running = true;

	while(m_running)
	{
		struct timeval ms;
		struct timeval tmp;
		struct timeval now;
		unsigned long int timeout;
		int ret;

		gettimeofday(&now, NULL);
		timerclear(&ms);

		/* search in the timeout queue for the next timer to trigger */
		SEventSet::iterator it = m_timers.begin();
		for( ; it != m_timers.end(); ++it)
		{
			Event &ev = m_events_pool[*it];
			if (!timerisset(&ms))
			{
				ms = ev.timeout;
			}
			else
			{
				if (timercmp(&(ev.timeout), &ms, <))
				{
					ms = ev.timeout;
				}
			}
		}

		/* 1s timeout if none has been set */
		if (!timerisset(&ms) || timercmp(&ms, &now, <) || timercmp(&ms, &now, ==))
		{
			timeout = 1000;
		}
		else
		{
			timersub(&ms, &now, &tmp);
			timeout = (tmp.tv_sec * 1000) + (tmp.tv_usec / 1000) + 1;
		}

		/* wait evnets and fire callback */
		ret = m_event_mod->wait(timeout);

		if(ret < 0)
		{
			return;
		}

		/* trigger timers */
		/* 为避免erase()之后迭代器失效问题，先把ev指针放到另一个set里。 */
		SEventSet ready_timers;
		for(it = m_timers.begin(); it != m_timers.end(); ++it)
		{
			Event &ev = m_events_pool[*it];

			gettimeofday(&now, NULL);

			if(NULL == ev.callback || timercmp(&now, &(ev.timeout), <))
			{
				continue;
			}

			ready_timers.insert(*it);
		}

		for(it = ready_timers.begin(); it != ready_timers.end(); ++it)
		{
			int ed = *it;
			Event &ev = m_events_pool[ed];

			ev.type = SEV_TIMEOUT;

			/* callback */
			(*ev.callback)(ed, SEV_TIMEOUT, ev.arg);

			/* 此timer可能在callback中被删除 */
			if(0 == m_timers.count(ed))
			{
				continue;
			}

			/* 如果这个timer未被删除，且它是持续性的，则更新一下它的timeout */
			if ((ev.flags & SEV_PERSIST))
			{
				timeradd(&now, &(ev.frequency), &(ev.timeout));
			}
			/* 否则，把它从timer列表中删除 */
			else
			{
				m_timers.erase(ed);
				if(0 == m_events.count(ed))
				{
					m_free_eds[m_free_top++] = ed;
				}
			}
		}
	}
}

void SEvent::stop(void)
{
	m_running = false;
}

