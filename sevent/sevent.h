/**
 * author: Soli <soli@cbug.org>
 * date  : 2012-05-23
 * */

#ifndef __SEVENT_H__
#define __SEVENT_H__

#include <set>
#include <sys/time.h>

#define SEV_TIMEOUT	(1 << 0)
#define SEV_READ    (1 << 1)
#define SEV_WRITE	(1 << 2)
#define SEV_PERSIST (1 << 3)
#define SEV_EDGE    (1 << 4)

struct Event;

typedef void (*SEventCallback)(int ed, int type, void * arg);

struct Event
{
	int ed;
	int fd;
	int flags;
	short type;
	struct timeval timeout;   /* next time to trigger */
	struct timeval frequency;
	SEventCallback callback;
	void *arg;
};

enum SEventModType
{
	SEMOD_EPOLL,
	SEMOD_SELECT

};

class SEventMod
{
public:
	SEventMod(){}
	virtual ~SEventMod(){}

	// Return the type of module
	virtual SEventModType type() = 0;

	// Init the module
	virtual int init(int max) = 0;

	// Clean the module
	virtual void clean(void) = 0;

	// Add an event to the set of monitored events
	virtual int add(Event * ev) = 0;

	// Mod an event in the set of monitored events
	virtual int mod(Event * ev) = 0;

	// Remove an event from the set of monitored events
	virtual int del(Event * ev) = 0;

	// Wait for events or timeout
	virtual int wait(int timeout) = 0;
};

class SEvent
{
public:
	SEvent();
	virtual ~SEvent();

	int  init(SEventModType type, int max);
	void clean(void);
	int  add(const Event &ev);
	int  mod(const Event &ev);
	int  del(int ed);
	void loop(void);
	void stop(void);

private:
	typedef std::set<int> SEventSet;

	Event *m_events_pool;
	int m_max_events;
	int *m_free_eds;
	int m_free_top;
	SEventSet m_events;
	SEventSet m_timers;
	SEventMod * m_event_mod;
	bool m_running;
};

Event make_timer(time_t timeout, bool persist, SEventCallback callback, void * arg);
Event make_event(int fd, int flags, SEventCallback callback, void * arg);

#endif	// #ifndef __SEVENT_H__

