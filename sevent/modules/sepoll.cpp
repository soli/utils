/**
 * author: Soli <soli@cbug.org>
 * date  : 2012-05-23
 * */

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "sepoll.h"


SEpoll::SEpoll()
{
	clean();
}

SEpoll::~SEpoll()
{
}

SEventModType SEpoll::type()
{
	return SEMOD_EPOLL;
}

int SEpoll::init(int max_fd)
{
	if (max_fd < 1)
	{
		return -1;
	}

	/* init epoll */
	m_epollfd = epoll_create(max_fd + 1);
	if (m_epollfd < 0)
	{
		return -2;
	}

	/* allocate fds */
	m_epollfds = (struct epoll_event *)malloc(sizeof(struct epoll_event) * max_fd);
	if (!m_epollfds)
	{
		return -3;
	}
	memset(m_epollfds, 0, sizeof(struct epoll_event) * max_fd);

	/* save max */
	m_nepollfds = max_fd;

	return 0;
}

void SEpoll::clean(void)
{
	/* free epollfds */
	if (m_epollfds)
	{
		free(m_epollfds);
		m_epollfds = NULL;
	}

	m_nepollfds = 0;

	/* close epollfd */
	if(m_epollfd >= 0)
	{
		close(m_epollfd);
		m_epollfd = -1;
	}
}

int SEpoll::add(Event * ev)
{
	struct epoll_event e;

	/* fill epoll struct */
	e.events = 0;
	e.data.ptr = (void *)ev;

	if(ev->flags & SEV_READ)
	{
		e.events = e.events | EPOLLIN;
	}

	if(ev->flags & SEV_WRITE)
	{
		e.events = e.events | EPOLLOUT;
	}

	if(ev->flags & SEV_EDGE)
	{
		e.events = e.events | EPOLLET;
	}

	/* add the event to epoll internal queue */
	if (epoll_ctl(m_epollfd, EPOLL_CTL_ADD, ev->fd, &e) == -1)
	{
		return -1;
	}

	return 0;
}

int SEpoll::mod(Event * ev)
{
	struct epoll_event e;

	/* fill epoll struct */
	e.events = 0;
	e.data.ptr = (void *)ev;

	if(ev->flags & SEV_READ)
	{
		e.events = e.events | EPOLLIN;
	}

	if(ev->flags & SEV_WRITE)
	{
		e.events = e.events | EPOLLOUT;
	}

	if(ev->flags & SEV_EDGE)
	{
		e.events = e.events | EPOLLET;
	}

	/* add the event to epoll internal queue */
	if (epoll_ctl(m_epollfd, EPOLL_CTL_MOD, ev->fd, &e) == -1)
	{
		return -1;
	}
	return 0;
}

int SEpoll::del(Event * ev)
{
	struct epoll_event e;

	/* fill epoll struct the same way we did in fpm_event_epoll_add() */
	e.events = 0;
	e.data.ptr = (void *)ev;

	if(ev->flags & SEV_READ)
	{
		e.events = e.events | EPOLLIN;
	}

	if(ev->flags & SEV_WRITE)
	{
		e.events = e.events | EPOLLOUT;
	}

	if (ev->flags & SEV_EDGE)
	{
		e.events = e.events | EPOLLET;
	}

	/* remove the event from epoll internal queue */
	if (epoll_ctl(m_epollfd, EPOLL_CTL_DEL, ev->fd, &e) == -1)
	{
		return -1;
	}

	return 0;
}

int SEpoll::wait(int timeout)
{
	int ret, i;

	/* ensure we have a clean epoolfds before calling epoll_wait() */
	memset(m_epollfds, 0, sizeof(struct epoll_event) * m_nepollfds);

	/* wait for inconming event or timeout */
	ret = epoll_wait(m_epollfd, m_epollfds, m_nepollfds, timeout);
	if (ret == -1)
	{
		/* trigger error unless signal interrupt */
		if (errno != EINTR)
		{
			return -1;
		}
		return 0;
	}

	/* events have been triggered, let's fire them */
	for (i = 0; i < ret; i++)
	{
		Event * ev = (Event*) m_epollfds[i].data.ptr;

		/* do we have a valid ev ptr ? */
		if (NULL == ev || NULL == ev->callback)
		{
			continue;
		}

		/* it is a read event */
		if(m_epollfds[i].events & EPOLLIN)
		{
			ev->type = SEV_READ;

			/* callback the event */
			(*ev->callback)(ev->ed, ev->type, ev->arg);
		}
		
		/* it is a write event */
		if(m_epollfds[i].events & EPOLLOUT)
		{
			ev->type = SEV_WRITE;

			/* callback the event */
			(*ev->callback)(ev->ed, ev->type, ev->arg);
		}
	}

	return ret;
}

