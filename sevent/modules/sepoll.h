/**
 * author: Soli <soli@cbug.org>
 * date  : 2012-05-23
 * */

#ifndef __SEPOLL_H__
#define __SEPOLL_H__

#include <sys/epoll.h>

#include "../sevent.h"

class SEpoll : public SEventMod
{
public:
	SEpoll();
	virtual ~SEpoll();

	// Return the type of module
	virtual SEventModType type();

	// Init the module
	virtual int init(int max_fd);

	// Clean the module
	virtual void clean(void);

	// Add a FD to the fd set
	virtual int add(Event * ev);

	// Mod a FD in the fd set
	virtual int mod(Event * ev);

	// Remove a FD from the fd set
	virtual int del(Event * ev);

	// Wait for events or timeout
	virtual int wait(int timeout);

private:
	struct epoll_event * m_epollfds;
	int m_nepollfds;
	int m_epollfd;
};

#endif	// #ifndef __SEPOLL_H__

