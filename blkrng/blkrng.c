/* blkrng.c - block ring buffer subroutine library */

/**
 * Copyright (c) 2009-2015, Soli <soli@cbug.org>
 * All rights reserved.
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * */

/*
modification history
--------------------
2009-11-02,Soli changed to the zlib license
2009-10-29,Soli update the email
2009-10-28,Soli	written
*/

/*
DESCRIPTION
--------------------
This library provides routines for creating and using ring buffers, which
are first-in-first-out circular buffers. The elementary unit of these
ring buffers is block, of which the size can be customized.
The routines simply manipulatethe ring buffer data structure;
no kernel functions are invoked.  Inparticular, ring buffers by themselves
provide no task synchronization or mutual exclusion.

However, the ring buffer pointers are manipulated in such a way that a
reader task (invoking blkrngGet()) and a writer task (invoking
blkrngPut()) can access a ring simultaneously without requiring mutual
exclusion.  This is only on condition that "=", "-=" and "+=" are atomic 
operations. 

However, access by multiple readers or writers MUST be interlocked
through a mutual exclusion mechanism (i.e., a mutual-exclusion semaphore
guarding a ring buffer).
*/

#include <stdlib.h>
#include <string.h>
/* add #include here */
#include "blkrng.h"

/**
 * blkrngCreate - create an empty ring buffer
 *
 * This routine creates a ring buffer, which consists of <blocks>
 * blocks, and each block contains <blksize> bytes.
 *
 * RETURNS:
 * The ID of the ring buffer, or NULL if memory cannot be allocated.
 * */
BLKRNG_ID blkrngCreate (
		UINT blocks,	/* number of blocks in ring buffer */
		UINT blksize	/* number of bytes in each block   */
		)
{
	BLKRNG_ID brId = (BLKRNG_ID) malloc( sizeof(BLKRNG) );

	if ( brId == NULL )
		return(NULL);

	brId->blkbuf = (char *) malloc ( blksize * blocks );

	if ( brId->blkbuf == NULL )
	{
		free( brId );
		return(NULL);
	}

	brId->blocks = blocks;
	brId->blksize = blksize;

	blkrngFlush( brId );

	return( brId );
}

/**
 * blkrngDelete - delete a ring buffer
 *
 * This routine deletes a specified ring buffer.
 * Any data currently in the buffer will be lost.
 *
 * RETURNS: N/A
 * */
void blkrngDelete (
		BLKRNG_ID brId	/* ring buffer to delete */
		)
{
	if ( brId == NULL )
		return;

	if ( brId->blkbuf != NULL )
		free( brId->blkbuf );

	free( brId );
}

/**
 * blkrngFlush - make a ring buffer empty
 *
 * This routine initializes a specified ring buffer to be empty.
 * Any data currently in the buffer will be lost.
 *
 * RETURNS: N/A
 * */
void blkrngFlush (
		BLKRNG_ID brId	/* ring buffer to initialize */
		)
{
	if ( brId == NULL )
		return;

	brId->head = 0;
	brId->tail = 0;
	brId->usedblks = 0;
}

/**
 * blkrngGet - get blocks from a ring buffer
 *
 * This routine copies blocks from the ring buffer <brId> into <blkbuf>.
 * It copies as many blocks as are available in the ring, up to <maxblks>.
 * The blocks copied will be removed from the ring.
 *
 * RETURNS:
 * The number of blocks actually received from the ring buffer;
 * it may be zero if the ring buffer is empty at the time of the call.
 * */
UINT blkrngGet (
		BLKRNG_ID brId,	/* ring buffer to get data from      */
		char *blkbuf,	/* pointer to buffer to receive data */
		UINT maxblks	/* maximum number of blocks to get   */
		)
{
	UINT tail = 0;
	UINT blks = 0;
	
	if (brId == NULL || blkbuf == NULL || maxblks < 1 || brId->usedblks < 1)
		return(0);

	tail = brId->tail;
	blks = brId->usedblks; /* hope it is atomic operation */

	if( blks < maxblks )
		maxblks = blks;

	/* step 1 */
	blks = maxblks < (brId->blocks - tail) ? maxblks : (brId->blocks - tail);
	
	memcpy( blkbuf, brId->blkbuf + tail * brId->blksize, blks * brId->blksize);

	blkbuf += blks * brId->blksize;

	tail = (tail + blks) % brId->blocks;

	/* step 2 */
	blks = maxblks - blks;

	if ( blks > 0 )
	{
		memcpy( blkbuf, brId->blkbuf + tail * brId->blksize, blks * brId->blksize);

		tail = (tail + blks) % brId->blocks;
	}

	brId->tail = tail;
	brId->usedblks -= maxblks; /* hope it is atomic operation */

	return( maxblks );
}

/**
 * blkrngPut - put blocks into a ring buffer
 *
 * This routine puts blocks from <blkbuf> into ring buffer <ringId>.
 * <nblks> blocks will be put into the ring, up to the number of free
 * blocks available in the ring.
 *
 * RETURNS:
 * The number of blocks actually put into the ring buffer;
 * it may be less than number requested, even zero,
 * if there is insufficient room in the ring buffer at the time of the call.
 * */
UINT blkrngPut (
		BLKRNG_ID brId,	/* ring buffer to put data into  */
		char *blkbuf,	/* buffer to get data from       */
		UINT nblks		/* number of blocks to try to put */
		)
{
	UINT head = 0;
	UINT blks = 0;
	
	if (brId == NULL || blkbuf == NULL 
		|| nblks < 1 || brId->usedblks >= brId->blocks)
		return(0);

	head = brId->head;
	blks = brId->usedblks; /* hope it is atomic operation */
	
	if ( brId->blocks - blks < nblks )
		nblks = brId->blocks - blks;

	/* step 1 */
	blks = nblks < (brId->blocks - head) ? nblks : (brId->blocks - head);
	
	memcpy( brId->blkbuf + head * brId->blksize, blkbuf, blks * brId->blksize);

	blkbuf += blks * brId->blksize;

	head = (head + blks) % brId->blocks;

	/* step 2 */
	blks = nblks - blks;

	if ( blks > 0 )
	{
		memcpy( brId->blkbuf + head * brId->blksize, blkbuf, blks * brId->blksize);

		head = (head + blks) % brId->blocks;
	}

	brId->head = head;
	brId->usedblks += nblks; /* hope it is atomic operation */

	return( nblks );
}

/**
 * blkrngIsEmpty - test if a ring buffer is empty
 *
 * This routine determines if a specified ring buffer is empty.
 *
 * RETURNS:
 * TRUE if empty, FALSE if not or the specified ring buffer is invalid.
 * */
BOOL blkrngIsEmpty (
		BLKRNG_ID brId	/* ring buffer to test */
		)
{
	if ( brId == NULL )
		return (FALSE);

	if ( brId->usedblks == 0 )
		return (TRUE);

	return (FALSE);
}

/**
 * blkrngIsFull - test if a ring buffer is full (no more room)
 *
 * This routine determines if a specified ring buffer is completely full.
 *
 * RETURNS:
 * TRUE if full or the specified ring buffer is invalid, FALSE if not.
 * */
BOOL blkrngIsFull (
		BLKRNG_ID brId	/* ring buffer to test */
		)
{
	if ( brId == NULL )
		return (TRUE);

	if ( brId->usedblks == brId->blocks )
		return (TRUE);

	return (FALSE);
}

/**
 * blkrngFreeBlks - determine the number of free blocks in a ring buffer
 *
 * This routine determines the number of blocks currently unused
 * in a specified ring buffer.
 *
 * RETURNS:
 * The number of unused blocks in the ring buffer.
 * */
UINT blkrngFreeBlks (
		BLKRNG_ID brId	/* ring buffer to examine */
		)
{
	if ( brId == NULL )
		return (0);

	return ( brId->blocks - brId->usedblks );
}

/**
 * blkrngNBlks - determine the number of blocks in a ring buffer
 *
 * This routine determines the number of blocks currently in a specified
 * ring buffer.
 *
 * RETURNS: The number of blocks filled in the ring buffer.
 * */
UINT blkrngNBlks (
		BLKRNG_ID brId	/* ring buffer to examine */
		)
{
	if ( brId == NULL )
		return (0);

	return ( brId->usedblks );
}

