/* blkrng.h - block ring buffer subroutine library header */

/**
 * Copyright (c) 2009-2015, Soli <soli@cbug.org>
 * All rights reserved.
 * 
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * */

/*
modification history
--------------------
2009-11-02,Soli changed to the zlib license
2009-10-29,Soli update the email
2009-10-28,Soli	written
*/

/*
DESCRIPTION
--------------------
block ring buffer subroutine library header
*/

/* typedefs */

#ifndef __INC_blkrng_h_Soli_
#define __INC_blkrng_h_Soli_

#ifdef __cplusplus
extern "C" {
#endif


/* add #include here */

#ifndef BOOL
#define BOOL int
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef UINT
#define UINT unsigned int
#endif

#ifndef NULL
#define NULL 0
#endif

typedef struct _blkrng	/* BLKRNG - block ring buffer */
{
	UINT blocks;	/* size of ring in blocks */
	UINT blksize;	/* size of block in bytes */

	UINT head;		/* offset from start of buffer where to write next */
	UINT tail;		/* offset from start of buffer where to read next  */
	UINT usedblks;	/* number of blocks filled(used) in the ring buffer*/

	char * blkbuf;	/* pointer to start of buffer */
} BLKRNG;

typedef BLKRNG* BLKRNG_ID;


/* function declarations */
extern BLKRNG_ID	blkrngCreate	( UINT blocks, UINT blksize );
extern void	blkrngDelete	( BLKRNG_ID brId );
extern void	blkrngFlush		( BLKRNG_ID brId );
extern UINT	blkrngGet		( BLKRNG_ID brId, char *blkbuf, UINT maxblks );
extern UINT	blkrngPut		( BLKRNG_ID brId, char *blkbuf, UINT nblks );
extern BOOL	blkrngIsEmpty	( BLKRNG_ID brId );
extern BOOL	blkrngIsFull	( BLKRNG_ID brId );
extern UINT	blkrngFreeBlks	( BLKRNG_ID brId );
extern UINT	blkrngNBlks		( BLKRNG_ID brId );



#ifdef __cplusplus
}
#endif

#endif /* #ifndef __INC_blkrng_h_Soli_ */

